<?php

namespace app\controllers;

use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class SiteController extends Controller {

	public function behaviors() {
		return [
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex() {
		if (Yii::$app->user->isGuest) {
			return $this->redirect('/web/auth/login');
		} else {
			return $this->redirect('/web/admin');
		}
	}

}
