<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			'id',
			'email',
			'phone',
			[
				'attribute' => 'status',
				'format' => 'raw',
				'value' => function ($data){
					return $data->getStatus();
				}
			],
			[
				'attribute' => 'product_id',
				'format' => 'raw',
				'value' => function ($data){
					return $data->getProduct()->title;
				}
			],
			'date',
			[
				'class' => 'yii\grid\ActionColumn',
				'header'=>'Действия', 
				'headerOptions' => ['width' => '80'],
				'template' => '{update} {delete}',
			],
		],
	]); ?>
</div>
