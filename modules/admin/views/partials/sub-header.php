<?php 
use yii\helpers\Url;

$navs = [];

$navs[] = [
  'label' => 'Главная',
  'url' => '/',
  'icon' => 'icon-dashboard',
  'active' => ($this->context->route == '/'),
];
$navs[] = [
  'label' => 'Товары',
  'url' => '/admin/product/index',
  'icon' => 'icon-list-alt',
  'active' => ($this->context->route == 'product/index'),
];
$navs[] = [
  'label' => 'Склады',
  'url' => '/admin/warehouse/index',
  'icon' => 'icon-shopping-cart',
  'active' => ($this->context->route == 'product/index'),
];

?>

<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <?php foreach ($navs as $item): ?>
          <li class="<?= $item['active'] ? 'active' : '' ?>">
            <a href="<?= Url::toRoute([$item['url']]) ?>"><i class="<?= $item['icon'] ?>"></i><span><?= $item['label'] ?></span> </a>
          </li>
        <?php endforeach ?>
<!--         <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Склады</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="/">Главный склад</a></li>
          </ul>
        </li>
 -->      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->