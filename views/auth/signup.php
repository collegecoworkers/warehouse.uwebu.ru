<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>

<link href="/public/css/pages/signin.css" rel="stylesheet" type="text/css">

<div class="account-container">
	<div class="content clearfix">
		<?php 
		$form = ActiveForm::begin();
		?>

		<?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

		<?= $form->field($model, 'email')->textInput() ?>

		<?= $form->field($model, 'password')->passwordInput() ?>

		<?= Html::a('Войти', ['auth/login' ], ['class' => 'button btn btn-large', 'style' => 'margin-left: 10px' ]) ?>
		<?= Html::submitButton('Отправить', ['class' => 'button btn btn-success btn-large' ]) ?>

		<?php ActiveForm::end(); ?>
	</div> <!-- /content -->
</div> <!-- /account-container -->
