<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войти';
$this->params['breadcrumbs'][] = $this->title;
?>

<link href="/public/css/pages/signin.css" rel="stylesheet" type="text/css">

<div class="account-container">
	<div class="content clearfix">
		<?php 
		$form = ActiveForm::begin();
		?>

		<?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

		<?= $form->field($model, 'password')->passwordInput() ?>

		<?= $form->field($model, 'rememberMe')->checkbox()
		?>

		<?= Html::a('Зарегистрироваться', ['auth/signup' ], ['class' => 'button btn btn-large', 'style' => 'margin-left: 10px' ]) ?>
		<?= Html::submitButton('Отправить', ['class' => 'button btn btn-success btn-large' ]) ?>

		<?php ActiveForm::end(); ?>
	</div> <!-- /content -->
</div> <!-- /account-container -->
