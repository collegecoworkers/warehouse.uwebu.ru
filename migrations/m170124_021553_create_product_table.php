<?php

use yii\db\Migration;
// error 
// don't migrate
/**
 * Handles the creation of table `product`.
 */
class m170124_021553_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'title'=>$this->string(),
            'description'=>$this->text(),
            'date'=>$this->date(),
            'image'=>$this->string(),
            'warehouse_id'=>$this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product');
    }
}
