<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "warehouse".
 *
 * @property integer $id
 * @property string $title
 */
class Warehouse extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'warehouse';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title'], 'string', 'max' => 255],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Title',
		];
	}

	public function getProducts()
	{
		return $this->hasMany(Product::className(), ['warehouse_id' => 'id']);
	}

	public function getProductsCount()
	{
		return $this->getProducts()->count();
	}
	
	public static function getAll($param = null)
	{
		if($param){
			// die;
			return Warehouse::find()->where($param)->all();
		} else{
			return Warehouse::find()->all();
		}
	}
	
	public static function getProductsByWarehouse($id)
	{
		// build a DB query to get all products
		$query = Product::find()->where(['warehouse_id'=>$id]);

		// get the total number of products (but do not fetch the product data yet)
		$count = $query->count();

		// create a pagination object with the total count
		$pagination = new Pagination(['totalCount' => $count, 'pageSize'=>6]);

		// limit the query using the pagination and retrieve the products
		$products = $query->offset($pagination->offset)
			->limit($pagination->limit)
			->all();

		$data['products'] = $products;
		$data['pagination'] = $pagination;
		
		return $data;
	}
}
