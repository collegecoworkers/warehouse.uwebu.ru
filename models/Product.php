<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

class Product extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'product';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['title'], 'required'],
			[['title','description'], 'string'],
			[['title'], 'string', 'max' => 255],
			[['date'], 'date', 'format'=>'php:Y-m-d'],
			[['date'], 'default', 'value' => date('Y-m-d')],
			[['warehouse_id'], 'number'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Заголовок',
			'description' => 'Описание',
			'date' => 'Дата создания',
			'warehouse_id' => 'Склад',
		];
	}

	public function saveProduct()
	{

		if(!$this->date) $this->date = date('Y-m-d');

		return $this->save(false);
	}

	public function getWarehouse()
	{
		return Warehouse::find()->where(['id' => $this->warehouse_id])->one();
	}

	public function getDate()
	{
		return Yii::$app->formatter->asDate($this->date);
	}
	
	public static function getAll($pageSize = 5)
	{
		// build a DB query to get all products
		$query = Product::find();

		// get the total number of products (but do not fetch the product data yet)
		$count = $query->count();

		// create a pagination object with the total count
		$pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);

		// limit the query using the pagination and retrieve the products
		$products = $query->offset($pagination->offset)
			->limit($pagination->limit)
			->all();
		
		$data['products'] = $products;
		$data['pagination'] = $pagination;
		
		return $data;
	}

	public function viewedCounter()
	{
		$this->viewed += 1;
		return $this->save(false);
	}

}
